import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main
{

    public static void main(String[] args)
    {
        QuickSort quickSort = new QuickSort();
        MergeSort mergeSort = new MergeSort();
        Random random = new Random();
        List<Integer> unsorted = new ArrayList<Integer>();

        for(int i = 0; i < 100000; i++)
        {
            Integer num = new Integer(random.nextInt());
            System.out.print(num + ", ");
            unsorted.add(num);
        }

//        quickSort.quickSort(unsorted, 0, unsorted.size()-1);
        unsorted = mergeSort.mergeSort(unsorted);

        for(int i = 0; i < unsorted.size(); i++)
        {
            System.out.println(unsorted.get(i) + ", ");
        }
    }
}
