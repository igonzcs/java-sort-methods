import java.util.ArrayList;
import java.util.List;

/**
 * Created By: Ismael Gonzalez
 * Date: 3/28/13
 * Time: 12:24 AM
 */
public class MergeSort
{
    public List<Integer> mergeSort(List<Integer> set)
    {
        if(set.size() <= 1)
        {
            return set;
        }

        List<Integer> left = new ArrayList<Integer>();
        List<Integer> right = new ArrayList<Integer>();

        Integer middle = set.size() / 2;

        for(int i = 0; i < middle; i++)
        {
            left.add(set.get(i));
        }

        for(int i = middle; i < set.size(); i++)
        {
            right.add(set.get(i));
        }

        left = mergeSort(left);
        right = mergeSort(right);

        return merge(left, right);
    }

    public List<Integer> merge(List<Integer> left, List<Integer> right)
    {
        List<Integer> result = new ArrayList<Integer>();
        while(left.size() > 0 || right.size() > 0)
        {
            if(left.size() > 0 && right.size() > 0)
            {
                if(left.get(0) <= right.get(0))
                {
                    result.add(left.remove(0));
                }
                else
                {
                    result.add(right.remove(0));
                }
            }
            else if(left.size() > 0)
            {
                result.add(left.remove(0));
            }
            else if(right.size() > 0)
            {
                result.add(right.remove(0));
            }
        }
        return result;
    }
}
