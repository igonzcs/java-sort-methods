import java.util.List;

/**
 * Created By: Ismael Gonzalez
 * Date: 3/27/13
 * Time: 11:50 PM
 */
public class QuickSort
{
    public void quickSort(List<Integer> set, int firstIndex, int lastIndex)
    {
        int q;
        if(firstIndex < lastIndex)
        {
            q = partition(set, firstIndex, lastIndex);
            quickSort(set, firstIndex, q - 1);
            quickSort(set, q + 1, lastIndex);
        }
    }

    public int partition(List<Integer> set, int firstIndex, int lastIndex)
    {
        Integer x = set.get(lastIndex);
        int i = firstIndex - 1;
        for(int j = firstIndex; j <= lastIndex - 1; j++)
        {
            if(set.get(j) <= x)
            {
                i += 1;
                Integer temp = set.get(i);
                set.set(i, set.get(j));
                set.set(j, temp);
            }
        }
        Integer temp = set.get(i + 1);
        set.set(i + 1, set.get(lastIndex));
        set.set(lastIndex, temp);
        return i + 1;
    }
}
